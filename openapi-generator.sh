#!/usr/bin/env bash

#version="4.2.0-SNAPSHOT"
version="5.0.0-SNAPSHOT"
jar="$HOME/.m2/repository/org/openapitools/openapi-generator-cli/$version/openapi-generator-cli-$version.jar"

if [ ! -f ${jar} ]; then
	echo "Error: ${jar} not found"
	exit 1
fi

java -ea                          \
  ${JAVA_OPTS}                    \
  -Xms512M                        \
  -Xmx1024M                       \
  -server                         \
  -jar ${jar} "$@"

